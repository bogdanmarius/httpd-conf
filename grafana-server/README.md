<p>Grafana server is configured locally available at URL: http://localhost:3000/ </p>

<p><br> By using Apache web server and setting up a reverse proxy with SSL support
<br>we can access Grafana at https://domain.tld/ or https://ip-address </p>
