# httpd-conf



## Getting started

Welcome, feel free to browse thru conf models. Adapt as needed. Test in development environment before production implementation.

## Cloning the repository


```
cd existing_repo
git clone https://gitlab.com/bogdanmarius/httpd-conf.git
cd httpd-conf
```


## Description
Public repository with models of Apache web server configuration files used to make reverse proxy setups for applications.
Location of config files:
```
/etc/httpd/conf.d/
```

## WARNING
Always test the configuration files and after that restart the webserver:
```
sudo apachectl configtest
sudo service httpd restart
sudo service httpd status
```
Have fun :)
