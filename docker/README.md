<p> The docker containers are running locally like this:
<br> http://127.0.0.1:3000/currency
<br> http://127.0.0.1:3000/chucknorris 
<br> No internet exposure </p>


<p> By setting up reverse proxy with Apache web server they become accessible to the internet
<br> http://domain.tld/currency
<br> http://domain.tld/chucknorris
</p>
