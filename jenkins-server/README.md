<p> In this scenario jenkins CI/CD Server is running inside a docker container

<p> On the host server the Apache Websever config location for AlmaLinux 8 is:
<br> <b>/etc/httpd/conf.d/</b>

<p> After the configuration file is created, the syntax check must be executed and the Websever restarted:
<br> sudo apachectl configtest
<br> sudo service httpd restart
<br> sudo service httpd status

<p> Jenkins becomes available at https://domain.tld or https://ip-address with an http to https redirect in place. 
