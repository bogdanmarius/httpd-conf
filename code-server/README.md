<p> By default the application runs on http://ip-address:8080/code

<br> Setting up reverse proxy the application is exposed to the internet using an SSL certificate

<br> Web URL: https://ip-address/code or https://domain.tld/code </p>
